(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/service/hopital.service.ts":
/*!********************************************!*\
  !*** ./src/app/service/hopital.service.ts ***!
  \********************************************/
/*! exports provided: HopitalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HopitalService", function() { return HopitalService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _utils_apputils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/apputils */ "./src/app/utils/apputils.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HopitalService = /** @class */ (function () {
    function HopitalService(http) {
        this.http = http;
    }
    HopitalService.prototype.getHopitals = function () {
        return this.http.get(_utils_apputils__WEBPACK_IMPORTED_MODULE_2__["AppUtils"].BASE_API_URL + '/uniteOrganisations');
    };
    HopitalService.prototype.getHopitalById = function (id) {
        try {
            return this.http.get(_utils_apputils__WEBPACK_IMPORTED_MODULE_2__["AppUtils"].BASE_API_URL + '/uniteOrganisations/' + id);
        }
        catch (error) {
            return null;
        }
    };
    HopitalService.prototype.createHopital = function (hopital) {
        return this.http.post(_utils_apputils__WEBPACK_IMPORTED_MODULE_2__["AppUtils"].BASE_API_URL + '/uniteOrganisations', hopital);
        //console.log(Hopital.libelle);
    };
    HopitalService.prototype.updateHopital = function (hopital) {
        return this.http.put(_utils_apputils__WEBPACK_IMPORTED_MODULE_2__["AppUtils"].BASE_API_URL + '/uniteOrganisations', hopital);
    };
    HopitalService.prototype.deleteHopital = function (id) {
        return this.http.delete(_utils_apputils__WEBPACK_IMPORTED_MODULE_2__["AppUtils"].BASE_API_URL + '/uniteOrganisations/' + id);
    };
    HopitalService.prototype.rechercherHopital = function (hopital) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set("code", hopital.code.valueOf())
            .set("nom", hopital.nom.valueOf());
        //alert(JSON.stringify(params))
        return this.http.get(_utils_apputils__WEBPACK_IMPORTED_MODULE_2__["AppUtils"].BASE_API_URL + '/rechercherUnite', { params: params });
    };
    HopitalService.prototype.getChargerUnite = function () {
        return this.http.get(_utils_apputils__WEBPACK_IMPORTED_MODULE_2__["AppUtils"].BASE_API_URL + '/chargerUniteOrganisation');
    };
    HopitalService.prototype.getChargerUniteCoordonnee = function () {
        return this.http.get(_utils_apputils__WEBPACK_IMPORTED_MODULE_2__["AppUtils"].BASE_API_URL + '/chargerUniteCoordonnees');
    };
    HopitalService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], HopitalService);
    return HopitalService;
}());



/***/ }),

/***/ "./src/app/utils/apputils.ts":
/*!***********************************!*\
  !*** ./src/app/utils/apputils.ts ***!
  \***********************************/
/*! exports provided: AppUtils */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppUtils", function() { return AppUtils; });
var AppUtils = /** @class */ (function () {
    function AppUtils() {
    }
    AppUtils.SERVER_PORT = "3000";
    AppUtils.CLIENT_PORT = "4200";
    //
    //public static readonly BASE_API_URL = "https://play.dhis2.org/dev/api";
    //public static readonly BASE_API_URL = "http://localhost:3000";
    AppUtils.BASE_API_URL = "https://mflapi.herokuapp.com";
    AppUtils.BASE_API_URL2 = "http://localhost:8888";
    AppUtils.BASE_BACK_URL = "admin-";
    AppUtils.LOADING_STATUS = "true fffffffffffffff";
    return AppUtils;
}());



/***/ })

}]);
//# sourceMappingURL=common.js.map