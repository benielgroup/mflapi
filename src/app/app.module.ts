import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BasicAuthInterceptor, ErrorInterceptor } from './_helpers';
import { NgxLoadingModule } from 'ngx-loading';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ChartsModule,
    NgxLoadingModule.forRoot({}),
    //MaterialModule,

    RouterModule.forRoot([
     /*  { path: '', component: AccueilComponent},
      { path: 'accueil', component: AccueilComponent},
      { path: 'hopital', component: HopitalComponent},
      { path: 'public-hopital', component: PhopitalComponent},


      { path: 'pharmacie', component: PharmacieListComponent},
      { path: 'pharmacie-ajout', component: PharmacieComponent},
      { path: 'laboratoire', component: LaboratoireListComponent},
      { path: 'laboratoire-ajout', component: LaboratoireComponent},


      //securite
      { path: 'profil', component: ProfilComponent},
      { path: 'profil-list', component: ProfilListComponent}, */

      {
        path: '',
        loadChildren: './back/back.module#BackModule'
      },
       {
        path: 'p',
        loadChildren: './public/public.module#PublicModule'
      } 
    ], {onSameUrlNavigation: 'reload'})
  ],
  providers: [
  //  { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
   // { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    fakeBackendProvider
],
  bootstrap: [AppComponent]
})
export class AppModule { }
