import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseListeComponent } from 'src/app/BaseListeComponent';
import { Laboratoire } from 'src/app/models/Laboratoire';
import { PharmarcieService } from 'src/app/service/laboratoire.service';

@Component({
  selector: 'app-laboratoire-list',
  templateUrl: './laboratoire-list.component.html',
  styleUrls: ['./laboratoire-list.component.css']
})
export class LaboratoireListComponent extends BaseListeComponent {

  pageTitre = "Liste des laboratoire";

  laboratoireList: Laboratoire[];
  laboratoireId: String;
  titre: String;


  constructor(private laboratoireService: PharmarcieService, private router: Router) {
    super();
  }


  ngOnInit() {
    this.laboratoireService.getLaboratoires()
      .subscribe( data => {
          this.laboratoireList = data;
      });
  }

  openModal(id: string) {
    this.laboratoireId = id;
  }

  deleteLaboratoire(laboratoire: Laboratoire): void {
    console.log("suppression de " + laboratoire.id);
    this.laboratoireService.deleteLaboratoire(laboratoire.id)
      .subscribe(data => {
        this.router.navigate(['admin-laboratoire']);
      })
  };

  editLaboratoire(laboratoire: Laboratoire): void {
    localStorage.removeItem("editLaboratoireId");
    localStorage.setItem("editLaboratoireId", laboratoire.id.toString());
    this.router.navigate(['laboratoire-ajouter']);
    console.log(localStorage);
  };


}