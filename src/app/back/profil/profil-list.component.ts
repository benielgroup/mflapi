import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Profil } from './Profil';
import { ProfilService } from 'src/app/service/profil.service';

@Component({
  selector: 'app-profil-list',
  templateUrl: './profil-list.component.html',
  styleUrls: ['./profil-list.component.css']
})
export class ProfilListComponent implements OnInit {

  profilList: Profil[];
  profilId:String;
  titre:String;
  constructor(private profilService:ProfilService, private router: Router) { }

  ngOnInit() {
    this.profilService.getProfils()
      .subscribe( data => {
          this.profilList = data;
          console.log(this.profilList)
      });
  }

  
  openModal(id: string) {
    this.profilId = id;
  }

  deleteProfil(profil: Profil): void {
    console.log("suppression de "+profil.id);
    this.profilService.deleteProfil(profil.id)
      .subscribe( data => {
        this.router.navigate(['admin-profil']);
      })
  };

  editProfil(profil: Profil): void {
    localStorage.removeItem("editProfilId");
    localStorage.setItem("editProfilId", profil.id.toString());
    this.router.navigate(['admin-profil-ajouter']);
    console.log(localStorage);
  };


}
