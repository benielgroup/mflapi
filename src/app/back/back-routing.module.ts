import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BackComponent } from './back.component';
import { AuthGuard } from '../_guards';
import { ProfilListComponent } from './profil/profil-list.component';
import { AccueilComponent } from '../accueil/accueil.component';
import { HopitalComponent } from './hopital/hopital.component';
import { PharmacieListComponent } from './pharmacie/pharmacie-list.component';
import { PharmacieComponent } from './pharmacie/pharmacie.component';
import { LaboratoireListComponent } from './laboratoire/laboratoire-list.component';
import { ProfilComponent } from './profil/profil.component';
import { LaboratoireComponent } from './laboratoire/laboratoire.component';
import { ConfigurationComponent } from './configuration/configuration.component';


const routes: Routes = [
  {
    path: '',
    component: BackComponent,
    // canActivate: [ AuthGuard ],
    children: [
      { path: '', redirectTo: 'accueil', pathMatch: 'full' },
      { path: 'profil', component: ProfilListComponent },
      { path: 'accueil', component: AccueilComponent },
      { path: 'hopital', component: HopitalComponent },


      { path: 'pharmacie', component: PharmacieListComponent },
      { path: 'pharmacie-ajout', component: PharmacieComponent },
      { path: 'laboratoire', component: LaboratoireListComponent },
      { path: 'laboratoire-ajout', component: LaboratoireComponent },


      //securite
      { path: 'profil', component: ProfilComponent },
      { path: 'profil-list', component: ProfilListComponent },

      //administration
      { path: 'configuration', component: ConfigurationComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackRoutingModule { }
