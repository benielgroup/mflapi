import { Component, OnInit } from '@angular/core';
import { BaseFormComponent } from 'src/app/BaseFormComponent';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Pharmacie } from 'src/app/models/Pharmacie';
import { RpReponse } from 'src/app/models/RpReponse';
import { PharmarcieService } from 'src/app/service/laboratoire.service';
import { Router } from '@angular/router';
import { BackComponent } from '../back.component';

@Component({
  selector: 'app-pharmacie',
  templateUrl: './pharmacie.component.html',
  styleUrls: ['./pharmacie.component.css']
})
export class PharmacieComponent extends BackComponent {

  pageTitre = "Pharmacie";
  public pharmacie = new Pharmacie();
  public rpReponse = new RpReponse();

  pharmacieForm: FormGroup;
  nom: FormControl;
  nomCourt: FormControl;
  contact: FormControl;
  adresse: FormControl;
  code: FormControl;
  dateOuverture: FormControl;
  dateFermeture: FormControl;

  submitted = false;


  constructor(public pharmarcieService: PharmarcieService,
    public router: Router) {
    super(router);
    this.loading = false;
  }

  getForm() {
    return this.pharmacieForm;
  }

  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.nom = new FormControl('', Validators.required);
    this.nomCourt = new FormControl('', Validators.required);
  }

  createForm() {
    this.pharmacieForm = new FormGroup({
      nom: this.nom,
      nomCourt: this.nomCourt,
    });


  }

  onSubmitForm() {
    this.submitted = true;
    if (this.pharmacieForm.invalid) {
      return;
    }
    alert('SUCCESS!! :-)')
  }


}
