import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BackRoutingModule } from './back-routing.module';
import { BackComponent } from './back.component';
import { AccueilComponent } from '../accueil/accueil.component';
import { ProfilComponent } from './profil/profil.component';
import { ProfilListComponent } from './profil/profil-list.component';
import { HopitalComponent } from './hopital/hopital.component';
import { PharmacieComponent } from './pharmacie/pharmacie.component';
import { PharmacieListComponent } from './pharmacie/pharmacie-list.component';
import { LaboratoireComponent } from './laboratoire/laboratoire.component';
import { LaboratoireListComponent } from './laboratoire/laboratoire-list.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { NgxLoadingModule } from 'ngx-loading';
import { MaterialModule } from '../material.module';

@NgModule({
  declarations: [
    BackComponent,
    AccueilComponent,
    ProfilComponent,
    ProfilListComponent,
    HopitalComponent,
    PharmacieComponent,
    PharmacieListComponent,
    LaboratoireComponent,
    LaboratoireListComponent,
    ConfigurationComponent,
  ],
  imports: [
    CommonModule,
    BackRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    NgxLoadingModule.forRoot({}),
    //MaterialModule,
  ]
})
export class BackModule { }
