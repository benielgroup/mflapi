import { Profil } from "src/app/classe/Profil";

export class Utilisateur{
    constructor( ){}

    public id:number;
    public nom:String;
    public prenom:String;
    public email:String;
    public motDePasse:String;
    public motDePasseConfirm:String;
    public actif:Boolean;
    public dateExpiration:Date;
    public idProfil:Number;
    public profil:Profil;
   
}