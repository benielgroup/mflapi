import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { HopitalComponent } from './phopital/hopital.component';
import { NgxLoadingModule } from 'ngx-loading';
import { TutoComponent } from './tuto/tuto.component';

@NgModule({
  declarations: [
    PublicComponent,
    HopitalComponent,
    TutoComponent,
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    NgxLoadingModule.forRoot({}),
  ]
})
export class PublicModule { }
