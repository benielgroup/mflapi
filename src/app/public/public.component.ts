import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css']
})
export class PublicComponent implements OnInit {


  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }



  protected createFormControls() {

  }

  protected createForm() {

  }


}
