export class Hopital{

    constructor( ){}
    public id:Number;
    public nom:String;
    public nomCourt:String;
    public code:String;
    public dateOuverture:Date;
    public dateFermeture:Date;
    public contact:String;
    public adresse:String;
   
}