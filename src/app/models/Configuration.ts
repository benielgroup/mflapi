export class Configuration {

    constructor() { }
    public id: Number;
    public apiUrl: String;
    public uniteDefaultQuery: String;
    public apiPassword: String;
    public apiLogin: String;
    public uniteChargee: Boolean;
}